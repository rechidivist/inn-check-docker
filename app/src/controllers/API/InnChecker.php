<?php

namespace Controller\API;

class InnChecker{
    
    function postIndex(){
        
        $inn = filter_var(Router::request()->getInputHandler()->value("inn"),FILTER_SANITIZE_NUMBER_INT);

        $isValid = self::innValidate($inn);
        
        if($isValid){
            $helper = new \Helper\RedisHelper();
            
            $response = $helper->getString($inn);
            if($response==null){
                $result = self::makeExternalRequest($inn);
                $helper->setStringWithTTL($inn,$result,86400);
                $response = $result;
            }
        }else{
            $response = json_encode(["message"=>"Инн не верен"], JSON_UNESCAPED_UNICODE);
        }
        Router::Response()->header('Content-Type: application/json; charset=utf-8');
        return $response;
    }

    function innValidate($inn){

        $isValid = false;
        $checkArray10=[2,4,10,3,5,9,4,6,8,0];
        $checkArray12_1=[7,2,4,10,3,5,9,4,6,8,0];
        $checkArray12_2=[3,7,2,4,10,3,5,9,4,6,8,0];

        if(strlen($inn) == 12){
            $summ = 0;

            for($i=0; $i<11;$i++){
                $summ += $inn[$i]*$checkArray12_1[$i];
            }

            $checksumm1 = $summ % 11;

            if($checksumm1 > 9){
                $checksumm1 %= 10;
            }

            $summ = 0;
            for($i=0; $i<12;$i++){
                $summ += $inn[$i]*$checkArray12_2[$i];
            }

            $checksumm2 = $summ % 11;
            if($checksumm2 > 9){
                $checksumm2 %= 10;
            }
            $isValid = $checksumm1 == $inn[10] && $checksumm2 == $inn[11];
        }

        return $isValid;
    }

    function makeExternalRequest($inn){
        $data = ['inn'=>$inn,"requestDate"=>date("Y-m-d")];
        $ch = curl_init('https://statusnpd.nalog.ru/api/v1/tracker/taxpayer_status');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE)); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);
        
        return $res;
    }
}