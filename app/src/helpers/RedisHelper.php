<?php

namespace Helper;

class RedisHelper{
    public $client = null;

    function __construct(){
        $this->client = new \Predis\Client('tcp://database:6379');
    }

    function getString($key){
        return $this->client->get($key);
    }

    function setStringWithTTL($key,$value,$secs){
        $this->client->set($key,$value);
        $this->client->expire($key,$secs);
    }

}