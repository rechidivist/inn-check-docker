<?php
use Pecee\SimpleRouter\SimpleRouter;

SimpleRouter::get('/', function() {
    return file_get_contents('./views/index.html');
});

SimpleRouter::get('/static{resource}', function($resource) {
    return file_get_contents('./static/');
});

SimpleRouter::group(['namespace'=>'API','prefix'=>'/api'], function() {
    SimpleRouter::controller('/inn', InnChecker::class);
});
