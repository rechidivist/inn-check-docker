document.getElementById('form').addEventListener('submit', e => {
	e.preventDefault();

	fetch('/api/inn', {
		method: 'POST',
		body: JSON.stringify({
			'inn': document.querySelector('input[type=number]').value
		}),
		headers: {'Content-Type': 'application/json'}
	}).then(resp => {
		return resp.json();
	}).then(data => {
		document.getElementById('message').innerHTML = data.message;
		document.getElementById('code').innerHTML = data.code ?? "";
	});
});